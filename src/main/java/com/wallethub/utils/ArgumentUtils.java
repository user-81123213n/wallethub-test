package com.wallethub.utils;

import static com.wallethub.constant.ParserConst.DIFF;

import com.wallethub.constant.ParserArgumentCount;
import com.wallethub.enums.Argument;
import com.wallethub.exception.ParserException;
import com.wallethub.parameter.ParameterOperation;
import java.util.Date;
import java.util.Map;

import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;

public final class ArgumentUtils {

    private ArgumentUtils() {

    }

    public static JobParameters getJobParameters(String[] args) throws ParserException {
        Map<Argument, Object> params = ParameterOperation.getParameters(args);
        return createJobParameters(params);
    }

    private static JobParameters createJobParameters(Map<Argument, Object> params) {
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        if (ParserArgumentCount.getCount() == 2) {
            jobParametersBuilder.addString(Argument.count.name(), (String) params.get(Argument.count));
        } else {
            jobParametersBuilder.addDate(Argument.startDate.name(), (Date) params.get(Argument.startDate));
            jobParametersBuilder.addDate(Argument.toDate.name(), (Date) params.get(Argument.toDate));
            jobParametersBuilder.addLong(Argument.threshold.name(), (long) params.get(Argument.threshold));
        }
        jobParametersBuilder.addString(Argument.accesslog.name(), (String) params.get(Argument.accesslog));
        jobParametersBuilder.addLong(DIFF, System.currentTimeMillis());
        return jobParametersBuilder.toJobParameters();
    }
}
