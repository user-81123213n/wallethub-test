package com.wallethub.utils;

import static com.wallethub.constant.ParserConst.HELP_MESSAGE;
import static com.wallethub.constant.ParserConst.SUPPRESS_DEFAULT_CONSTRUCTOR_MESSAGE;

public final class ConsoleWriter {

    private ConsoleWriter() {
        throw new AssertionError(SUPPRESS_DEFAULT_CONSTRUCTOR_MESSAGE);
    }

    public static void printHelp() {
        System.out.println(HELP_MESSAGE);
    }
}
