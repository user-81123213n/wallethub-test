package com.wallethub.entity;

import static lombok.AccessLevel.PRIVATE;

import java.util.Date;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.FieldDefaults;

@Data
@NoArgsConstructor
@AllArgsConstructor
@FieldDefaults(level = PRIVATE)
public class LogItem {

    Long id;
    short status;
    Date timeCreated;
    String ip;
    String version;
    String browser;
}
