package com.wallethub.config;

import com.wallethub.constant.ParserArgumentCount;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class JobConfiguration {

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Autowired
    private JobCompletionNotificationListener jobCompletionListener;

    @Autowired
    @Qualifier("logItemStep")
    private Step logItemStep;

    @Autowired
    @Qualifier("banItemStep")
    private Step banItemStep;

    @Autowired
    @Qualifier("logOperationStep")
    private Step logOperationStep;

    @Bean
    public Job processLogJob() {
        if (ParserArgumentCount.getCount() == 5) {
            return jobBuilderFactory.get("processLogJob")
                    .incrementer(new RunIdIncrementer())
                    .listener(jobCompletionListener)
                    .flow(logItemStep)
                    .next(banItemStep)
                    .end()
                    .build();
        } else {
            return jobBuilderFactory.get("processLogJob")
                    .incrementer(new RunIdIncrementer())
                    .listener(jobCompletionListener)
                    .flow(logItemStep)
                    .next(logOperationStep)
                    .end()
                    .build();
        }
    }
}
