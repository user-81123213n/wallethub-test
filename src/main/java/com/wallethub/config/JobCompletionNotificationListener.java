package com.wallethub.config;

import static com.wallethub.constant.ParserConst.JOB_COMPLETION_NOTIFICATION_LISTENER_FAILED;
import static com.wallethub.constant.ParserConst.JOB_COMPLETION_NOTIFICATION_LISTENER_FINISHED;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class JobCompletionNotificationListener extends JobExecutionListenerSupport {

    @Override
    public void afterJob(JobExecution jobExecution) {
        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
            log.info(String.format(JOB_COMPLETION_NOTIFICATION_LISTENER_FINISHED, jobExecution.getId()));
        } else if (BatchStatus.FAILED == jobExecution.getStatus()) {
            log.error(String.format(JOB_COMPLETION_NOTIFICATION_LISTENER_FAILED, jobExecution.getId()));
        }
    }
}
