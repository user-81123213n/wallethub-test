package com.wallethub.config;

import static java.util.Arrays.asList;

import com.wallethub.entity.BanItem;
import com.wallethub.entity.LogBanItem;
import com.wallethub.enums.Argument;
import com.wallethub.processor.BanItemProcessor;
import com.wallethub.constant.Queries;
import java.sql.Timestamp;
import java.util.Date;
import java.util.Map;
import javax.sql.DataSource;

import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcCursorItemReader;
import org.springframework.batch.item.support.CompositeItemWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.BeanPropertyRowMapper;

@Configuration
public class BanItemConfiguration {

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private DataSource dataSource;

    @Autowired
    @Qualifier("jdbcReader")
    private JdbcCursorItemReader jdbcReader;

    @Autowired
    private BanItemProcessor banItemProcessor;

    @Bean("banItemStep")
    public Step banItemStep() {
        return stepBuilderFactory.get("banItemStep")
                .<LogBanItem, BanItem>chunk(50)
                .reader(jdbcReader)
                .processor(banItemProcessor)
                .writer(banItemWriter())
                .build();
    }

    @Bean("jdbcReader")
    @StepScope
    public JdbcCursorItemReader reader(@Value("#{jobParameters}") Map<String, ?> jobParameters) {
        JdbcCursorItemReader reader = new JdbcCursorItemReader();
        reader.setDataSource(dataSource);
        reader.setSql(Queries.SELECT_LOG_ITEMS_SQL);
        reader.setRowMapper(new BeanPropertyRowMapper<>(LogBanItem.class));
        reader.setPreparedStatementSetter(ps -> {
            ps.setTimestamp(1, getTimeStamp((Date) jobParameters.get(Argument.startDate.name())));
            ps.setTimestamp(2, getTimeStamp((Date) jobParameters.get(Argument.toDate.name())));
            ps.setLong(3, (Long) jobParameters.get(Argument.threshold.name()));
        });
        return reader;
    }

    @Bean
    public CompositeItemWriter<BanItem> banItemWriter() {
        CompositeItemWriter<BanItem> writer = new CompositeItemWriter<>();
        writer.setDelegates(asList(
                jdbcWriter()
        ));
        return writer;
    }

    @Bean
    public JdbcBatchItemWriter<BanItem> jdbcWriter() {
        JdbcBatchItemWriter<BanItem> ipWriter = new JdbcBatchItemWriter<>();
        ipWriter.setSql(Queries.INSERT_BAN_ITEM_SQL);
        ipWriter.setDataSource(dataSource);
        ipWriter.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<>());
        return ipWriter;
    }

    private static Timestamp getTimeStamp(Date date) {
        return new Timestamp(date.getTime());
    }
}
