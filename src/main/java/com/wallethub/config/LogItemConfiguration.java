package com.wallethub.config;

import static com.wallethub.constant.ParserConst.PIPE_LINE;

import com.wallethub.dto.LogItemDTO;
import com.wallethub.entity.LogItem;
import com.wallethub.enums.Argument;
import com.wallethub.enums.LogItemColumn;
import com.wallethub.processor.LogItemProcessor;
import com.wallethub.constant.Queries;
import java.util.Map;
import javax.sql.DataSource;

import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.database.BeanPropertyItemSqlParameterSourceProvider;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.batch.item.file.LineMapper;
import org.springframework.batch.item.file.mapping.BeanWrapperFieldSetMapper;
import org.springframework.batch.item.file.mapping.DefaultLineMapper;
import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.DelimitedLineTokenizer;
import org.springframework.batch.item.file.transform.LineTokenizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.FileSystemResource;

@Configuration
public class LogItemConfiguration {

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private LogItemProcessor logItemProcessor;

    @Autowired
    private FlatFileItemReader<LogItemDTO> logReader;

    @Autowired
    private DataSource dataSource;

    @Bean("logItemStep")
    public Step logItemStep() {
        return stepBuilderFactory.get("logItemStep")
                .<LogItemDTO, LogItem>chunk(500)
                .reader(logReader)
                .processor(logItemProcessor)
                .writer(writer())
                .build();
    }

    @Bean
    @StepScope
    public FlatFileItemReader<LogItemDTO> reader(@Value("#{jobParameters}") Map<String, ?> jobParameters) {
        FlatFileItemReader<LogItemDTO> reader = new FlatFileItemReader<LogItemDTO>();
        reader.setResource(new FileSystemResource((String) jobParameters.get(Argument.accesslog.name())));
        reader.setLineMapper(createDefaultLineMapper());
        return reader;
    }

    @Bean
    public JdbcBatchItemWriter<LogItem> writer() {
        JdbcBatchItemWriter<LogItem> writer = new JdbcBatchItemWriter<LogItem>();
        writer.setDataSource(dataSource);
        writer.setSql(Queries.INSERT_LOG_ITEM_SQL);
        writer.setItemSqlParameterSourceProvider(new BeanPropertyItemSqlParameterSourceProvider<LogItem>());
        return writer;
    }

    private LineMapper<LogItemDTO> createDefaultLineMapper() {
        DefaultLineMapper<LogItemDTO> mapper = new DefaultLineMapper<LogItemDTO>();
        mapper.setLineTokenizer(createDelimitedLineTokenizer());
        mapper.setFieldSetMapper(createFieldSetMapper());
        return mapper;
    }

    private LineTokenizer createDelimitedLineTokenizer() {
        DelimitedLineTokenizer tokenizer = new DelimitedLineTokenizer(PIPE_LINE);
        tokenizer.setNames(new String[]{LogItemColumn.TIME.name(), LogItemColumn.IP.name(),
                LogItemColumn.VERSION.name(), LogItemColumn.STATUS.name(), LogItemColumn.BROWSER.name()});
        return tokenizer;
    }

    private FieldSetMapper<LogItemDTO> createFieldSetMapper() {
        BeanWrapperFieldSetMapper<LogItemDTO> mapper = new BeanWrapperFieldSetMapper<LogItemDTO>();
        mapper.setTargetType(LogItemDTO.class);
        return mapper;
    }
}
