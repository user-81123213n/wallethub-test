package com.wallethub.processor;

import static com.wallethub.constant.ParserConst.DATE_PATTERN;
import static org.apache.commons.lang3.time.DateUtils.parseDate;

import com.wallethub.dto.LogItemDTO;
import com.wallethub.entity.LogItem;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.stereotype.Component;

@Component
public class LogItemProcessor implements ItemProcessor<LogItemDTO, LogItem> {

    @Override
    public LogItem process(final LogItemDTO dto) throws Exception {
        LogItem item = new LogItem();
        item.setIp(dto.getIp());
        item.setStatus(Short.parseShort(dto.getStatus()));
        item.setTimeCreated(parseDate(dto.getTime(), DATE_PATTERN));
        item.setVersion(dto.getVersion());
        item.setBrowser(dto.getBrowser());
        return item;
    }
}
