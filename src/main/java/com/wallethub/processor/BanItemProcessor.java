package com.wallethub.processor;

import static com.wallethub.constant.ParserConst.REASON_MESSAGE;

import com.wallethub.entity.BanItem;
import com.wallethub.entity.LogBanItem;
import com.wallethub.enums.Argument;
import com.sun.javafx.binding.StringFormatter;
import java.util.Map;

import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@StepScope
public class BanItemProcessor implements ItemProcessor<LogBanItem, BanItem> {

    @Value("#{jobParameters}")
    private Map<String, ?> jobParameters;

    @Override
    public BanItem process(final LogBanItem log) {
        BanItem item = new BanItem();
        item.setIp(log.getIp());
        item.setReason(StringFormatter.format(REASON_MESSAGE, log.getTotal(),
                jobParameters.get(Argument.threshold.name())).getValue());
        return item;
    }
}
