package com.wallethub.enums;

public enum Duration {

    HOURLY, DAILY
}
