package com.wallethub.enums;

public enum LogItemColumn {

    TIME, IP, VERSION, STATUS, BROWSER
}
