package com.wallethub.enums;

public enum Argument {

    startDate, toDate, duration, threshold, accesslog, count
}
