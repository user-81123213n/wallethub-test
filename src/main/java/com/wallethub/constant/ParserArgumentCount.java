package com.wallethub.constant;

import static com.wallethub.constant.ParserConst.SUPPRESS_DEFAULT_CONSTRUCTOR_MESSAGE;

public final class ParserArgumentCount {

    private static int count;

    private ParserArgumentCount() {
        throw new AssertionError(SUPPRESS_DEFAULT_CONSTRUCTOR_MESSAGE);
    }

    public static final void setCount(int count) {
        ParserArgumentCount.count = count;
    }

    public static final int getCount() {
        return count;
    }
}
