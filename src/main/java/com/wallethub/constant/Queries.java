package com.wallethub.constant;

public interface Queries {

    String INSERT_LOG_ITEM_SQL = "INSERT INTO log (ip, time_created, version, status, browser)"
            + " VALUES (:ip, :timeCreated, :version, :status, :browser)";

    String INSERT_BAN_ITEM_SQL = "INSERT INTO ban_ip (ip, reason) VALUES (:ip, :reason)";

    String SELECT_LOG_ITEMS_SQL = "SELECT ip, COUNT(id) AS total FROM log"
            + " WHERE time_created between ? AND ?"
            + " GROUP by ip"
            + " HAVING COUNT(id) > ?";

    String SELECT_LOG_ITEM_BY_IP = "SELECT COUNT(id) FROM log WHERE ip=?";
}
