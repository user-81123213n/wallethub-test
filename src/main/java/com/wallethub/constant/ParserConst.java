package com.wallethub.constant;

public interface ParserConst {

    String DASH = "-";
    String EQUALS = "=";
    String PIPE_LINE = "|";
    String HELP = "--help";
    String DIFF = "duration";
    String COUNT = "the total request made by the ip: %d";
    String DATE_PATTERN = "yyyy-MM-dd hh:mm:ss.SSS";
    String DATE_CONSOLE_PATTERN = "yyyy-MM-dd.HH:mm:ss";
    String REASON_MESSAGE = "requested %S , reach limit requests %S";
    String INVALID_OPTION_MESSAGE = "Invalid option, please --Argument";
    String JOB_COMPLETION_NOTIFICATION_LISTENER_FAILED = "Job: %d, =========== JOB FAILED =========== ";
    String JOB_COMPLETION_NOTIFICATION_LISTENER_FINISHED = "Job: %d, ===========  JOB FINISHED =========== ";
    String PLEASE_ENTER_AN_ARGUMENTS_MESSAGE = "Please enter an arguments, for help type --help";
    String INVALID_NUMBER_OF_ARGUMENT = "Please enter the correct number of arguments, for help type --help";
    String HELP_MESSAGE = "--START_DATE=YYYY-MM-dd.HH:mm:ss"
            + "\n--DURATION=HOURLY|DAILY "
            + "\n--THRESHOLD=Maximum number of request"
            + "\n--ACCESS=path to ACCESS.log"
            + "\n--count=192.168.169.194 will get the total count of the request made by this ip"
            + "\nex: java -jar parser.jar --startDate=2017-01-01.00:00:00 --duration=daily --threshold=500 --accesslog=C:\\Users\\pc\\Desktop\\access.log"
            + "\nex: java -jar parser.jar --count=192.168.169.194";
    String SUPPRESS_DEFAULT_CONSTRUCTOR_MESSAGE = "Suppress default constructor for non-instantiability";
}
