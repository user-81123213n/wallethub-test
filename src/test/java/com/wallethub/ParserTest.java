package com.wallethub;

import com.wallethub.constant.Queries;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.File;

import static org.hamcrest.CoreMatchers.equalTo;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = MainCtl.class)
public class ParserTest extends BaseTest {

    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    private ApplicationContext context;

    @Before
    public void setUp() {
        file = new File(ParserTest.class.getClassLoader()
                .getResource(LOG_FILE).getFile());
    }

    @Test
    public void givenParserWhenDurationDailyThenExecute() throws Exception {
        // Arrange
        JobParameters jobParameters = createJobParametersForParserWhenDurationDaily();

        // Act & Assert
        JobLauncher launcher = context.getBean(JobLauncher.class);
        JobExecution result = launcher.run(context.getBean(Job.class), jobParameters);

        assertCommon(result);
    }

    @Test
    public void givenParserWhenIpFoundThenReturnsCount() throws Exception {
        // Arrange
        JobParameters jobParameters = createJobParametersForParserWhenIp(IP);

        // Act & Assert
        JobLauncher launcher = context.getBean(JobLauncher.class);
        JobExecution result = launcher.run(context.getBean(Job.class), jobParameters);
        Long actual = jdbcTemplate.queryForObject(Queries.SELECT_LOG_ITEM_BY_IP, new Object[]{IP}, Long.class);

        assertCommon(result);
        collector.checkThat(actual, equalTo(6L));
    }

    @Test
    public void givenParserWhenIpNotFoundThenReturnsZero() throws Exception {
        // Arrange
        JobParameters jobParameters = createJobParametersForParserWhenIp(DUMMY_STING);

        // Act & Assert
        JobLauncher launcher = context.getBean(JobLauncher.class);
        JobExecution result = launcher.run(context.getBean(Job.class), jobParameters);
        Long actual = jdbcTemplate.queryForObject(Queries.SELECT_LOG_ITEM_BY_IP, new Object[]{DUMMY_STING}, Long.class);

        assertCommon(result);
        collector.checkThat(actual, equalTo(0L));

    }
}
