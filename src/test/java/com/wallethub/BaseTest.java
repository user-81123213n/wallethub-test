package com.wallethub;

import com.wallethub.enums.Argument;
import com.wallethub.enums.Duration;
import org.junit.Rule;
import org.junit.rules.ErrorCollector;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;

import java.io.File;
import java.text.ParseException;

import static com.wallethub.constant.ParserConst.DATE_PATTERN;
import static org.apache.commons.lang3.time.DateUtils.parseDate;
import static org.hamcrest.CoreMatchers.equalTo;

public abstract class BaseTest {

    protected static final long THRESHOLD = 2L;
    protected static final String IP = "192.168.234.82";
    protected static final String DUMMY_STING = "test";
    protected static final String LOG_FILE = "access.log";
    protected static final String FROM_DATE = "2017-01-01 00:00:11.763";
    protected static final String TO_DATE = "2017-01-01 00:00:12.763";

    protected File file;

    @Rule
    public final ErrorCollector collector = new ErrorCollector();

    protected void assertCommon(JobExecution result) {
        result.getStepExecutions().forEach(s -> {
            collector.checkThat(result.getStatus(), equalTo(BatchStatus.COMPLETED));
        });
    }

    protected JobParameters createJobParametersForParserWhenIp(String ip) {
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addString(Argument.count.name(), ip);
        assertCreateJobParametersCommon(jobParametersBuilder);
        return jobParametersBuilder.toJobParameters();
    }

    protected JobParameters createJobParametersForParserWhenDurationDaily() throws ParseException {
        JobParametersBuilder jobParametersBuilder = new JobParametersBuilder();
        jobParametersBuilder.addDate(Argument.startDate.name(), parseDate(FROM_DATE, DATE_PATTERN));
        jobParametersBuilder.addDate(Argument.toDate.name(), parseDate(TO_DATE, DATE_PATTERN));
        jobParametersBuilder.addString(Argument.duration.name(), Duration.DAILY.name());
        jobParametersBuilder.addLong(Argument.threshold.name(), THRESHOLD);
        assertCreateJobParametersCommon(jobParametersBuilder);
        return jobParametersBuilder.toJobParameters();
    }

    protected void assertCreateJobParametersCommon(
            JobParametersBuilder jobParametersBuilder) {
        jobParametersBuilder.addString(Argument.accesslog.name(), file.getAbsolutePath());
    }
}
