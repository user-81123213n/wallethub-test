![Scheme](http://www.iphoneglance.com/wp-content/uploads/2016/07/wallethub-580x333.jpg)

## Wallethub Assignment Test

The Technologies that I have used in the assignment Spring(Boot, Batch, JDBC).

#### The Dependencies needed by the assignment
    1. Install Java 1.8: ![Java](https://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)
    2. Install MySQL then create a database named with `assignment`: ![MySQL](https://dev.mysql.com/downloads/)
    3- Username of the database: root
    4- Password of the database: root
you can change the URL string to the database in `application.properties`

#### How to run:
You can open the CMD or terminal and type the following commannd
for finding all IPs that made requests more than threshold between the time
`java -jar parser.jar --startDate=2017-01-01.13:00:00 --duration=hourly --threshold=100 --accesslog=E:\path\access.log`

#### What you will expected from the assignment:
    1- Source code
    2- Testing to the source code



